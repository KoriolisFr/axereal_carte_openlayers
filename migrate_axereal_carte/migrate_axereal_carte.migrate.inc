<?php
/**
 * @file
 * Declares our migrations.
 */
    

/**
 * Implements hook_migrate_api().
 */
function migrate_axereal_carte_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'openlayers' => array(
        'title' => t('Migrations for openlayers map'),
      ),
    ),
    'migrations' => array(
      'Sites' => array(
        'class_name' => 'SitesMigration',
        'group_name' => 'openlayers',
        'dependencies' => array(
          'Taxos',
        ),
      ),
      'Taxos' => array(
        'class_name' => 'TaxosMigration',
        'group_name' => 'openlayers',
      ),
    ),
  );
  return $api;
}
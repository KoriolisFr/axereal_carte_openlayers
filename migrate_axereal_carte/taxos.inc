<?php

class TaxosMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $path = DRUPAL_ROOT . '/' . drupal_get_path('module','migrate_axereal_carte') . '/taxos.csv';
    $csvcolumns = array();
    $options = array(
     'header_rows' => 1,
     'delimiter' => ';'
    );

    $this->source = new MigrateSourceCSV($path, $csvcolumns, $options);

    $this->destination = new MigrateDestinationTerm('type_site', array('allow_duplicate_terms' => TRUE));

    $source_key = array(
      'ID' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      )
    );
    $this->map = new MigrateSQLMap(
      $this->machineName,
      $source_key,
      MigrateDestinationTerm::getKeySchema()
    );

    $this->addFieldMapping('name', 'Taxo');
    $this->addFieldMapping('parent_name', 'Parent');
    $this->addFieldMapping('field_picto_carte', 'Picto');
    $this->addFieldMapping('field_picto_carte:source_dir')->defaultValue('/tmp/pictos');
    $this->addFieldMapping('field_picto_carte:destination_dir')->defaultValue('public://images/pictos');
  }
}
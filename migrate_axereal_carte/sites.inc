<?php

class SitesMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    $path = DRUPAL_ROOT . '/' . drupal_get_path('module','migrate_axereal_carte') . '/sites.csv';
    $csvcolumns = array();
    $options = array(
     'header_rows' => 1,
     'delimiter' => ';'
    );

    $this->source = new MigrateSourceCSV($path, $csvcolumns, $options);

    $this->destination = new MigrateDestinationNode('axereal_carte_site');

    $source_key = array(
      'ID' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      )
    );
    $this->map = new MigrateSQLMap(
      $this->machineName,
      $source_key,
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'Branche');
    $this->addFieldMapping('latitude_site', 'Latitude');
    $this->addFieldMapping('longitude_site', 'Longitude');
    $this->addFieldMapping('societe_site', 'Société');
    $this->addFieldMapping('adresse1_site', 'Adresse 1');
    $this->addFieldMapping('adresse2_site', 'Adresse 2');
    $this->addFieldMapping('cp_site', 'CP');
    $this->addFieldMapping('ville_site', 'Ville');
    $this->addFieldMapping('pays_site', 'Pays');
    $this->addFieldMapping('tel_site', 'Tel');
    $this->addFieldMapping('type_site', 'ID_for_taxo')->sourceMigration('Taxos');
    $this->addFieldMapping('type_site:source_type')->defaultValue('tid');
  }
}
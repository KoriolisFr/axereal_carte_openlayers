(function ($) {
  Drupal.behaviors.create_openlayers = {
    attach: function (context, settings) {
      var map_active_tids = [];
      var map_active_paragraph_tids = [];
      var map_coord = [18, 48];
      if (!Drupal.settings.taxoMenuBlock) {
        $("#map").after(Drupal.settings.taxoMenu);
      } else {
        $("#map").after(Drupal.settings.taxoMenuBlock);
      }

      var lang = $('html').attr('lang');
      if (lang == 'fr') {
        var url_lang_prefix = '';
      }
      if (lang == 'en-gb') {
        var url_lang_prefix = '/en';
      }
      if (lang == 'nl') {
        var url_lang_prefix = '/nl';
      }

      var styleFunction = function (feature) {
        var picto = feature.get('Picto_parent');
        if (picto == "") {
          picto = feature.get('Picto');
        }

        if (feature.get('Type_parent') == "Agricole" || picto == "") {
          // Trick with transparent background to still enable mouse over/click detection on marker
          var squareBackgroundStyle = new ol.style.Style({
            image: new ol.style.RegularShape({
              fill: new ol.style.Fill({
                color: [255, 255, 255, 0.01]
              }),
              points: 4,
              radius: 5
            })
          });
          var iconStyle = new ol.style.Style({
            image: new ol.style.Circle({
              fill: new ol.style.Fill({
                color: [250, 164, 50, 1]
              }),
              radius: 5
            })
          });
        } else {
          // Trick with transparent background to still enable mouse over/click detection on marker
          var squareBackgroundStyle = new ol.style.Style({
            image: new ol.style.RegularShape({
              fill: new ol.style.Fill({
                color: [255, 255, 255, 0.01]
              }),
              points: 4,
              radius: 12.5
            })
          });
          var iconStyle = new ol.style.Style({
            image: new ol.style.Icon({
              scale: 0.1,
              src: picto,
              size: [250, 250]
            })
          });
        }

        /*
       Ajout de condition pour le cas ou la carte soit dans un pragraph
        */
        if (map_active_paragraph_tids.length == 0) {
          if ($("#taxo-menu-map input:checked").length == 0) {
            return [squareBackgroundStyle, iconStyle];
          } else if ($.inArray(feature.get('Tid'), map_active_tids) != -1) {
            return [squareBackgroundStyle, iconStyle];
          }
        } else {
          if ($.inArray(feature.get('Tid'), map_active_paragraph_tids) != -1 && map_active_tids.length == 0) {
            return [squareBackgroundStyle, iconStyle];
          } else if ($.inArray(feature.get('Tid'), map_active_tids) != -1) {
            return [squareBackgroundStyle, iconStyle];
          }
        }

      };

      var styleCountries = function (feature, resolution) {
        return new ol.style.Style({
          fill: new ol.style.Fill({
            color: '#CCCCCC'
          }),
          stroke: new ol.style.Stroke({
            color: '#FFFFFF',
            width: 1
          }),
        });
      };

      var countriesLayer = new ol.layer.Tile({
        source: new ol.source.XYZ({
          url: 'https://api.mapbox.com/styles/v1/mapbox/streets-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoibGVvYzg2IiwiYSI6ImNqN2oyZXIwbTFqMXUyd3FrNGc3dHI0NTIifQ.9sdVfHRGUZktTgjwe0h34Q'
        })
      });


      var pointsLayerSource = new ol.source.Vector({
        url: url_lang_prefix + '/geojsoncarte.json',
        format: new ol.format.GeoJSON()
      });
      var pointsLayer = new ol.layer.Vector({
        title: 'points-interet',
        source: pointsLayerSource,
        style: styleFunction
      });

      /*pointsLayerSource.once('change', function(evt){
        if (pointsLayerSource.getState() === 'ready') {
          if (pointsLayerSource.getFeatures().length > 0) {
            map.getView().fit(pointsLayerSource.getExtent(), map.getSize());
          }
        }
      });*/

      var container = document.getElementById('popup');
      var content = document.getElementById('popup-content');
      var closer = document.getElementById('popup-closer');

      /**
       * Create an overlay to anchor the popup to the map.
       */
      var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
        element: container,
        autoPan: true,
        autoPanAnimation: {
          duration: 250
        }
      }));

      /**
       * Add a click handler to hide the popup.
       * @return {boolean} Don't follow the href.
       */
      if (closer != null) {
        closer.onclick = function () {
          overlay.setPosition(undefined);
          closer.blur();
          return false;
        };
      }


      /*function displayFeatureInfo(pixel) {
          info.style.left = pixel[0] + 'px';
          info.style.top = (pixel[1] - 50) + 'px';
          var feature = map.forEachFeatureAtPixel(pixel, function(feature, layer) {
              return feature;
          });
          if (feature &&  feature.getGeometry().getType() == 'Point') {
              var Picto = feature.get('Picto');
              var Type = feature.get('Type_parent');
              if (Type == '') {
                Type = feature.get('Type');
              }
              var description = feature.get('description');
              var Societe = feature.get('Societe');
              var Adresse_1 = feature.get('Adresse 1');
              var Adresse_2 = feature.get('Adresse 2');
              var CP = feature.get('CP');
              var Ville = feature.get('Ville');
              var Pays = feature.get('Pays');
              var Tel = feature.get('Tel');
              var html = '';
              html += '<h2>' + Societe + '</h2>';
              html += '<p>' + '<img src="' + Picto + '" style="width:40px;margin-left:6px;vertical-align: middle;" />' + '</p>';
              html += '<h4>' + Type + '</h4>';
              html += '<p>';
              if(Adresse_1 != '') {
                html += Adresse_1 + '<br />';
              }
              if(Adresse_2 != '') {
                html +=  Adresse_2 + '<br />';
              }
              if(CP != '') {
                html += CP + '<br />';
              }
              if(Ville != '') {
                html += Ville + '<br />';
              }
              if(Pays != '') {
                html += Pays + '<br />';
              }
              if(Tel != '') {
                html += Tel + '<br />';
              }
              html += '</p>';
              info.style.display = 'none';
              info.innerHTML = html;
              info.style.display = 'block';
              target.style.cursor = "pointer";
          } else {
              info.style.display = 'none';
              target.style.cursor = "";
          }
      }*/

      var zoomInfos = 2;
      if (Drupal.settings.info_para) {
        var infosPara = Drupal.settings.info_para;
        if (infosPara.zoom) zoomInfos = infosPara.zoom;
        if (infosPara.country) map_coord = infosPara.country;
      }

      var map = new ol.Map({
        layers: [
          countriesLayer,
          pointsLayer
        ],
        overlays: [overlay],
        target: 'map',
        controls: ol.control.defaults({
          attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
            collapsible: false
          })
        }),
        view: new ol.View({
          center: ol.proj.transform([map_coord[0], map_coord[1]], 'EPSG:4326', 'EPSG:3857'),
          minZoom: 2,
          zoom: zoomInfos
        })
      });

      if (lang == 'en-gb') {
        var brancheTranslations = {};
        brancheTranslations["Élevage"] = "Livestock Farming";
        brancheTranslations["Malterie"] = "Malting plant";
        brancheTranslations["Métiers du grain"] = "Grain Business";
        brancheTranslations["Meunerie"] = "Milling";
        brancheTranslations["Négoce International"] = "International Trade";
      }

      /**
       * Add a click handler to the map to render the popup.
       */
      map.on('singleclick', function (evt) {
        var coordinate = evt.coordinate;
        var pixel = map.getEventPixel(evt.originalEvent);
        var feature = map.forEachFeatureAtPixel(pixel, function (feature, layer) {
          return feature;
        });
        if (feature && feature.getGeometry().getType() == 'Point') {
          var Picto = feature.get('Picto_parent');
          if (Picto == '') {
            Picto = feature.get('Picto');
          }
          var Type_parent = feature.get('Type_parent');
          var Tid_parent = feature.get('Tid_parent');
          if (Tid_parent > 0) {
            if (Drupal.settings.taxoTranslations && typeof Drupal.settings.taxoTranslations[Tid_parent] !== "undefined") {
              var Type = Drupal.settings.taxoTranslations[Tid_parent];
            } else {
              var Type = Type_parent;
            }
          } else {
            var Type = feature.get('Type_site');
          }
          if (Tid_parent == 106 || Picto == "") {
            Picto_html = '<p class="agricole"><span class="picto"></span></p>';
          } else {
            Picto_html = '<p>' + '<img src="' + Picto + '" style="width:40px;margin-left:6px;vertical-align: middle;" />' + '</p>';
          }
          var Edit = feature.get('EditLink');
          var branche = feature.get('name');
          var Societe = feature.get('Societe');
          var Adresse_1 = feature.get('Adresse 1');
          var Adresse_2 = feature.get('Adresse 2');
          var CP = feature.get('CP');
          var Ville = feature.get('Ville');
          var Pays = feature.get('Pays');
          var Tel = feature.get('Tel');
          var LienVideo = feature.get('Lien video');
          var html = '';

          if (lang == 'en-gb') {
            if (typeof brancheTranslations[branche] !== "undefined") {
              branche = brancheTranslations[branche]
            }
          }
          // if (lang == 'nl') {
          //   if (typeof brancheTranslations[branche] !== "undefined") {
          //     branche = brancheTranslations[branche]
          //   }
          // }

          if (Edit != '') {
            html += '<span class="edit">' + Edit + '</span>';
          }
          html += '<h2>' + Type + '</h2>';
          html += Picto_html;
          html += '<h4>' + branche + '</h4>';
          html += '<h4>' + Societe + '</h4>';
          html += '<p>';
          if (Adresse_1 != '') {
            html += Adresse_1 + '<br />';
          }
          if (Adresse_2 != '') {
            html += Adresse_2 + '<br />';
          }
          if (CP != '') {
            html += CP + '<br />';
          }
          if (Ville != '') {
            html += Ville + '<br />';
          }
          if (Pays != '') {
            html += Pays + '<br />';
          }
          if (Tel != '') {
            html += Tel + '<br />';
          }
          if (LienVideo != null) {
            html += LienVideo + '<br />';
          }
          html += '</p>';
          content.innerHTML = html;
          overlay.setPosition(coordinate);
        }
      });


      /*map.on('click', function(evt) {
        if (evt.dragging) {
            info.style.display = 'none';
            return;
        }
        var pixel = map.getEventPixel(evt.originalEvent);
        displayFeatureInfo(pixel);
      });

      map.on('pointermove', function(e) {
        if (e.dragging) {
          info.style.display = 'none';
          target.style.cursor = "";
          return;
        }
        var pixel = map.getEventPixel(e.originalEvent);
        var feature = map.forEachFeatureAtPixel(pixel, function(feature, layer) {
            return feature;
        });
        if (feature &&  feature.getGeometry().getType() == 'Point') {
            target.style.cursor = "pointer";
        } else {
            info.style.display = 'none';
            target.style.cursor = "";
        }
      });*/

      $("#taxo-menu-map input").change(function (event) {
        map_active_tids = [];
        event.stopPropagation();
        $(this).parent("li").find("ul li input").prop("checked", $(this).prop("checked"));
        $("#taxo-menu-map input:checked").each(function () {
          map_active_tids.push($(this).data("tid"));
        });
        pointsLayer.setStyle(styleFunction);
      });

      if( !$('#overlay').length ){
        $('#taxo-menu-map form').masonry({
          itemSelector: '.legend-column',
          gutter: 10,
          fitWidth: true,
        });
      }

      if( $('#taxo-menu-map form .legend-column').length == 3 ) $('#taxo-menu-map form .legend-column').css('width', 'calc(33% - 10px)');
      if( $('#taxo-menu-map form .legend-column').length == 2 ) $('#taxo-menu-map form .legend-column').css('width', 'calc(50% - 10px)');
      if( $('#taxo-menu-map form .legend-column').length == 1 ) $('#taxo-menu-map form .legend-column').css('width', 'calc(100% - 10px)');

      /*
      *Recupere les sites que l'on souhaite voir apparaitre en affichage paragraph
     */
      if (Drupal.settings.info_para) {
        var sites = Drupal.settings.info_para.sltd_sites;
        for (var i = 0; i < sites.length; i++) {
          map_active_paragraph_tids.push(parseInt(sites[i].tid));
        }
      }

    }
  };
})(jQuery);